<?php
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    // maak een logboek
    $log = new \ModernWays\Helpers\LogApp('nl_NL');
    $connection = new \ModernWays\Helpers\Connection($log);
    $connection->setDatabaseName('WWWET');
    $connection->setPassword('xxxxxxxxxxxxxxxxxxx');
    $connection->setUserName('arthur');
    $connection->setHostName('localhost:3306');
    $connection->open();
    $connection->close();
 ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Connection class test</title>
    </head>
    <body>
    <?php include('../src/View/Feedback.php'); ?>
    </body>
</html>
