<?php
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
    $log = new \ModernWays\Helpers\LogApp('fr_FR');
    $session = new ModernWays\Helpers\Session($log);
    $session->start('ModernWays', false);
    // check if token is valid
    if ($session->isValidToken('test')) {
        $message = 'Valid token bestaat';
    }
    else {
        // create token
        $session->setToken('test');
        $message = 'Nieuwe Token ingesteld';
    }
    // $session->end();
    echo 'test' . $log->sessionUnsafe('beveiligde', 'test');
    
?>

<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta charset="utf-8" />
        <title>Session Test</title>
    </head>
    <body>
        <p>
            <?php echo $message;?>
        </p>
        <?php include('../src/View/Feedback.php'); ?>
    </body>
</html>


