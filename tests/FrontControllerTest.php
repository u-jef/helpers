<?php
/**
 * Created by Modern Ways.
 * Template: Jef Inghelbrecht
 * Date: 19/07/2015
 * Time: 11:25
 */
require '../../../../vendor/autoload.php';
$log = new \ModernWays\Helpers\Log();
$fc = new \ModernWays\Helpers\FrontController($log);
$fc->setUseCase('Feedback-createOne--clause_1');
$fc->createController("\\ModernWays\\Helpers\\");
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<?php include('../src/View/Feedback.php'); ?>
</body>
</html> 