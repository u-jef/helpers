helpers
==========

Helpers is the base library for components developped by ModernWays. 
The library contains classes that are used in all the ModernWays components.

Highlights
-------

* Connection.php
* ConnectionTest.php
* Feedback.php
* Locale.php
* Log.php
* LogApp.php
* Composer ready, [PSR-2] and [PSR-4] compliant

Documentation
-------

Full documentation (in dutch) can be found at [inantwerpen.com](http://www.inantwerpen.com).

System Requirements
-------

You need **PHP >= 5.4.0**.

Install
-------

Install `ModernWays\Helpers` using Composer.

```
$ composer require modernways/helpers dev-master
```

Configuration
-------

** no configuration required


Testing
-------

There is sample/test for each helper class:

Contributing
-------

Contributions are welcome and will be fully credited. Mail to jef.inghelbrecht@telenet.be.

Security
-------

If you discover any security related issues, please email jef.inghelbrecht@telenet.be instead of using the issue tracker.

Credits
-------


License
-------

The MIT License (MIT). Please see [LICENSE](LICENSE) for more information.

[PSR-2]: http://www.php-fig.org/psr/psr-2/
[PSR-4]: http://www.php-fig.org/psr/psr-4/