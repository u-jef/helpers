<?php
namespace ModernWays\Helpers;
class Locale extends \MessageFormatter
{
    
    protected $locale;
    protected $list;

    public function __construct($locale = 'en_US')
    {
        $this->locale = $this->locale;
        $this->list['en_US'] =
                array('CONNECTION' => 
                    "Connection to host {0} to {1} database {2, select, e {already done} s {succesfull} other {failed}}.");

        $this->list['fr_FR'] =
            array('CONNECTION' => 
                "Connexion avec hôte {0} avec banque de données {1} {2, select, e {déjà établie} s {réussie} other {ratée}}.");

        $this->list['nl_NL'] =
            array('CONNECTION' => 
                "Verbinding met host {0} met database {1} {2, select, e {al gemaakt} s {geslaagd} other {mislukt}}.");
    
    }

    public function connectionFailed($hostName, $databaseName)
    {
        echo $this->formatMessage($this->locale, $this->list[$this->locale]['CONNECTION'], array($hostName, $databaseName, 'f'));
    }

    public function connectionSuccesfull($hostName, $databaseName)
    {
        return $this->formatMessage($this->locale, $this->list[$this->locale]['CONNECTION'], array($hostName, $databaseName, 's'));
    }

    public function connectionEstablished($hostName, $databaseName)
    {
        return $this->formatMessage($this->locale, $this->list[$this->locale]['CONNECTION'], array($hostName, $databaseName, 'e'));
    }
}

