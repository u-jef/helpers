<?php
namespace ModernWays\Helpers;
class Session
{
    protected $name;
    protected $https;
    // if true, this stops JavaScript being able to access the session id.
    protected $httpOnly;
    protected $salt;

    protected $log;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isHttps()
    {
        return $this->https;
    }

    /**
     * @param boolean $https
     */
    public function setHttps($https)
    {
        $this->https = $https;
    }

    public function __construct($log, $name = 'ModernWays', $https = FALSE)
    {
        $this->name = $name;
        $this->https = $https;
        $this->salt = 'En mijn oom staat op de bergen, hali, halo.';
        $this->log = $log;
        // This stops JavaScript being able to access the session id.
        $this->httpOnly = true;
    }

    function __destruct()
    {
    }

    public function start()
    {
        $this->log->startTimeInKey('start session');
        if (session_status() === PHP_SESSION_NONE) {
            // Forces sessions to only use cookies.
            if (ini_set('session.use_only_cookies', 1) === FALSE) {
                echo 'Could not initiate a safe session (ini_set)';
                exit();
            }
            // Gets current cookies params.
            $cookieParams = session_get_cookie_params();
            // specifies the lifetime of the cookie in seconds which is sent to the browser.
            // The value 0 means "until the browser is closed." Defaults to 0.
            session_set_cookie_params($cookieParams["lifetime"],
                $cookieParams["path"],
                $cookieParams["domain"],
                $this->https,
                $this->httpOnly);
            // Sets the session name to the one set above.
            session_name($this->name);
            // session_save_path('d:\bin\temp');
            session_start(); // Start the php session
            $_SESSION['generated'] = time();
            // $this->regenerate();
            $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_STARTED);
            $this->log->setText($text);
            $this->log->log();
        } else {
            $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_ALREADY_STARTED);
            $this->log->setText($text);
            $this->log->log();
            $this->regenerate(true);
        }
    }

    function isStarted()
    {
        if (session_status() === PHP_SESSION_NONE) {
            return false;
        }
        return true;
    }

    public function makeTicket($value)
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        // hash the password with the unique salt.
        // XSS protection as we might print this value
        // session stays for one week valid
        return hash('sha512', $this->salt . $value . $userAgent);
    }

    public function isTicketSet()
    {
        if (isset($_SESSION['t'])) {
            return true;
        }
        return false;
    }

    public function setTicket($value)
    {
        $_SESSION['t'] = $this->makeTicket($value);
    }

    public function isValidTicket($value)
    {
        $this->log->startTimeInKey('start session');
        if (isset($_SESSION['t'])) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $ticket = $_SESSION['t'];
            // session stays for one week valid
            if ($ticket == hash('sha512', $this->salt . $value . $userAgent)) {
                $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_TICKET_VALIDATED);
                $this->log->setText($text);
                $this->log->log();
                return true;
            }
        }
        $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_TICKET_INVALIDATED);
        $this->log->setText($text);
        $this->log->log();
        return false;
    }

    public function setPositiveInteger($key, $value)
    {
        // XSS protection as we might print this value
        $_SESSION[$key] = preg_replace("/[^0-9]+/", "", $value);
    }

    public function setText($key, $value)
    {
        // XSS protection as we might print this value
        $_SESSION[$key] = preg_replace("/[^a-zA-Z0-9_ \-]+/", "", $value);
    }

    public function set($key, $value)
    {
        if (isset($_SESSION)) {
            $_SESSION[$key] = $value;
        }
    }

    public function get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return FALSE;
    }

    public function regenerate($deleteOldSession = true)
    {
        $this->log->startTimeInKey('regenerate session');
        if (!isset($_SESSION['generated']) || $_SESSION['generated'] < (time() - 20)) {
            // true will delete old session
            session_regenerate_id($deleteOldSession);
            $_SESSION['generated'] = time();
            $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_REGENERATED);
        } else {
            $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_NOT_REGENERATED);
        }
        $this->log->setText($text);
        $this->log->log();
    }

    public function end()
    {
        // session is started in the constructor
        $this->log->startTimeInKey('end session');
        $this->log->setDebugInfo();

        // Unset all session values
        $_SESSION = array();
        // get session parameters
        $params = session_get_cookie_params();
        // Delete the actual cookie.
        setcookie(session_name(), '', time() - 42000,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httponly']);
        // Destroy session
        session_destroy();
        $text = $this->log->session($this->https, $this->name, LogLocale::SESSION_ENDED);
        $this->log->setText($text);
        $this->log->setContext('ModernWays Session class');
        $this->log->log();
    }
}


