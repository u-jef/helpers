<?php
/**
 * Created by ModernWays.
 * Template: Jef Inghelbrecht
 * Date: 18/07/2015
 * Time: 20:53
 * The Front Controller consolidates all request handling by channeling requests
 * through a single handler object. This object can carry out common behavior,
 * which can be modified at runtime with decorators.
 * The handler then dispatches to command objects for behavior particular
 * to a request. (Martin Fowler)
 *
 * In our sample we intercept a get or a post as a usecase definition.
 * The usecase is first parse. Then a controller is dynamically created
 * and the appropriated method is called.
 */
namespace ModernWays\Helpers\Controller;
/**
 * Class UseCase
 * @package ModernWays\Helpers
 */
class FrontController
{

    protected $useCase; /* Use Case object */
    protected $namespace;
    protected $actionMethod;
    protected $viewMethod;

    protected $logApp; /* log book for application feedback */

    public function setUseCase($value)
    {
        $this->useCase = $value;
    }

    /**
     * @return mixed
     */
    public function getActionMethod()
    {
        return $this->actionMethod;
    }

    /**
     * @return mixed
     */
    public function getViewMethod()
    {
        return $this->viewMethod;
    }

    function __construct($logApp, $useCase = null)
    {
        $this->logApp = $logApp;
        $this->useCase = $useCase;
    }

    public function requestUseCase($default = '-home--')
    {
        // what action to take?
        // if uc (use case) is specified on querystring
        // change default action
        if (isset($_POST['uc'])) {
            $this->useCase->setName($_POST['uc']);
        } elseif (isset($_GET['uc'])) {
            // sommige acties komen via een get binnen op de url
            $this->useCase->setName($_GET['uc']);
        }
        else {
            // default action is home
            $this->useCase->setName($default);
        }
    }

    public function createController($appNamespace, $provider)
    {
        $this->namespace = $appNamespace;
        $controllerName = "$appNamespace\\Controller\\{$this->useCase->getEntity()}";
        // echo $controllerName;

        $this->actionMethod = new \ReflectionMethod("$appNamespace\\Controller\\{$this->useCase->getEntity()}", $this->useCase->getAction());
        $this->viewMethod = new \ReflectionMethod("$appNamespace\\View\\{$this->useCase->getEntity()}", $this->useCase->getAction());

        if (!class_exists($controllerName, true)) {

            $this->logApp->startTimeInKey('Create Controller');
            $this->logApp->setText("Creating the action controller '$controllerName'...");
            $this->logApp->setErrorMessage("The action controller '$controllerName' has not been defined.");
            $this->logApp->end();
            $this->logApp->log();
            return false;
        }
        else {
            $reflection = new \ReflectionClass($controllerName);
            $controller =  $reflection->newInstance($this->logApp, $provider, $this->useCase);

            $this->logApp->startTimeInKey('Create Controller');
            $this->logApp->setText("Creating the action controller '$controllerName'...");
            $this->logApp->setErrorMessage("The action controller '$controllerName' object has been created.");
            $this->logApp->end();
            $this->logApp->log();
            return $controller;
        }
    }
}