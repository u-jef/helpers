<?php
/**
 * Created by Modern Ways.
 * User: Jef Inghelbrecht
 * Date: 9/08/2015
 * Time: 15:53
 */
namespace ModernWays\Helpers\Controller;
class Base
{
    protected $useCase;
    protected $provider;
    protected $logApp;
    protected $logEntity;
    protected $view;

    /**
     * @return \ModernWays\Membership\View\User
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param mixed $logApp
     */
    public function setLogApp($logApp)
    {
        $this->logApp = $logApp;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param mixed $logEntity
     */
    public function setLogEntity($logEntity)
    {
        $this->logEntity = $logEntity;
    }

    public function __construct($logApp = null, $provider = null, $useCase)
    {
        // log for Application
        $this->logApp = $logApp;
        $this->logEntity = $logApp;
        $this->useCase = $useCase;

        // connection
        $this->provider = $provider;
        $this->setup();
    }

    public function __destruct()
    {
    }
}
