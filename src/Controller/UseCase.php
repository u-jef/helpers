<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 8/08/2015
 * Time: 18:21
 */

namespace ModernWays\Helpers\Controller;
/**
 * Class name
 * @package ModernWays\Helpers
 */
class UseCase
{
    protected $entity; /* the noun of the use case, object or instance */
    protected $id; /* unique identifier for entity as defined in use case*/
    protected $action; /* the verb of the use case */
    protected $clause;
    protected $name; /* the name of the use case */

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getClause()
    {
        return $this->clause;
    }

    /**
     * @param mixed $clause
     */
    public function setClause($clause)
    {
        $this->clause = $clause;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->parse();
    }

    function __construct($logApp)
    {
        $this->logApp = $logApp;
    }

    /* further specification of use case */

    public function isName()
    {
        return (!is_null($this->name));
    }

    public function isAction()
    {
        return (!is_null($this->action));
    }

    public function isClause()
    {
        return (!is_null($this->clause));
    }

    public function parse()
    {
        // look for Id
        $pos = strpos($this->name, '_');
        if ($pos !== FALSE) {
            $this->id = substr($this->name, $pos + 1);
            $this->name = substr($this->name, 0, $pos);
        }
        // if no clause defined, add empty one
        // action from one - up to double --
        $pos = strpos($this->name, '--');
        if ($pos == FALSE) {
            $this->name = $this->name . '--';
        }

        // look for action
        $pos = strpos($this->name, '--');
        $startPos = strpos($this->name, '-');
        $this->action = substr($this->name, $startPos + 1, $pos - $startPos - 1);

        // look for entity (table) name, up to first -
        $pos = strpos($this->name, '-');
        if ($pos !== FALSE) {
            $this->entity = substr($this->name, 0, $pos);
        }

        // clause from double -- to end
        $pos = strpos($this->name, '--');
        if ($pos !== FALSE) {
            $this->clause = substr($this->name, $pos + 2);
        }
        $this->logApp->startTimeInKey('set Use Case');
        $this->logApp->setText("Entitity = {$this->entity} / Action = {$this->action} / Clause = {$this->clause} / Id = {$this->id}");
        $this->logApp->setErrorMessage("IsActionSet = {$this->isAction()}");
        $this->logApp->end();
        $this->logApp->log();
    }
}
