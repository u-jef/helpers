<?php
/**
 * Created by ModernWays
 * Template: Jef Inghelbrecht
 * Date: 14/07/2015
 * Time: 22:41
 * output from the database
 * input is user input
 */
namespace ModernWays\Helpers;
class Identity
{
    private $session;
    private $loginAttempts;
    private $log;
    private $isVerified;

    public function setSession($value)
    {
        $this->session = $value;
        $this->log->startTimeInKey($this->log->identity(LogLocale::$identitySignIn));
        if (session_status() == PHP_SESSION_NONE) {
            $this->session->start();
            $this->log->setText($this->log->session($this->session->isHttps,
                $this->session->getName(), LogLocale::SESSION_OPENED_IN_LOGIN));
        } else {
            $this->log->setText($this->log->session($this->session->isHttps(),
                $this->session->getName(), LogLocale::SESSION_OPENED_OUTSIDE_LOGIN));
        }
        $this->log->log();
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

    public function setLoginAttempts($value)
    {
        $this->loginAttempts = $value;
    }

    public function getUserName()
    {
        return $this->session->get('UserName');
    }

    public function getUserId()
    {
        return $this->session->get('UserId');
    }

    public function __construct($log, $session)
    {
        $this->log = $log;
        $this->setSession($session);
        $this->loginAttempts = 0;
        $this->isVerified = FALSE;
    }

    private function verifyPassword($passwordInput, $hashedPasswordOutput)
    {
        $this->log->startTimeInKey($this->log->identity(LogLocale::$identitySignIn));
        if (password_verify($passwordInput, $hashedPasswordOutput)) {
            $this->log->setText($this->log->identity(LogLocale::$identityPasswordVerified));
            $this->log->log();
            return TRUE;
        }
        $this->log->setText($this->log->identity(LogLocale::$identityPasswordNotVerified));
        $this->log->log();
        return false;
    }

    private function verifyUser($userNameOutput)
    {
        $this->log->startTimeInKey($this->log->identity(LogLocale::$identitySignIn));
        if ($userNameOutput && $this->getUserName()) {
            if ($userNameOutput == $this->getUserName()) {
                $this->log->setText($this->log->identity(LogLocale::$identityUserVerified));
                $this->log->log();
                return TRUE;
            }
        }
        $this->log->setText($this->log->identity(LogLocale::$identityUserNotVerified));
        $this->log->log();
        return false;
    }

    private function verifyTicket($hashedPasswordOutput)
    {
        $this->log->startTimeInKey($this->log->identity(LogLocale::$identitySignIn));
        $ticket = $this->session->get('t');
        $ticketToCheck = $this->session->makeTicket($hashedPasswordOutput);
        if ($ticket == $ticketToCheck) {
            $this->log->setText($this->log->identity(LogLocale::$identityTicketVerified));
             $this->log->log();
            return TRUE;
        }
        $this->log->setText($this->log->identity(LogLocale::$identityTicketNotVerified));
        $this->log->log();
        return false;
    }

    private function isBruteForceAttack()
    {
        $this->log->startTimeInKey($this->log->identity(LogLocale::$identitySignIn));
        if ($this->loginAttempts > 10) {
            $this->log->setText($this->log->identity(LogLocale::$identityMaxLoginAttempts));
             $this->log->log();
            return TRUE;
        }
        $this->log->setText($this->log->identity(LogLocale::$identityNotMaxLoginAttempts));
        $this->log->log();
        return false;
    }

    public function login($userIdOutput, $userNameOutput, $passwordInput, $hashedPasswordOutput)
    {
        $this->log->startTimeInKey($this->log->identity(LogLocale::$identitySignIn));
        // de gebruiker wordt opgehaald buiten de login klasse
        // we weten al dat hij/zij bestaat
        if ($this->isBruteForceAttack()) {
            // user is locked out
            return FALSE;
        } else {
            if ($this->verifyPassword($passwordInput, $hashedPasswordOutput)) {
                // create Ticket
                $this->session->setTicket($hashedPasswordOutput);
                $this->session->setPositiveInteger('UserId', $userIdOutput);
                $this->session->setText('UserName', $userNameOutput);
                return TRUE;
            } else { return FALSE;
            }
        }
    }

    public function isLoggedIn($userNameOutput, $hashedPasswordOutput)
    {
        if ($this->verifyUser($userNameOutput)) {
            if ($this->verifyTicket($hashedPasswordOutput)) {
                // logged in
                $this->loggedIn = TRUE;
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            // gebruiker niet geverifieerd
            return FALSE;
        }
    }
}



