<?php
/**
 * Created by Modern Ways.
 * Template: Jef Inghelbrecht
 * Date: 4/08/2015
 * Time: 12:58
 */
namespace ModernWays\Helpers\Model;
class Base
{
    /*
     * - An entity may be concrete (a person or a book, for example) or 
     *   abstract (like a holiday or a concept).
     * - An entity set is a set of entities of the same type:
     *   (e.g., all persons having an account at a bank). 

     */
    protected $entity;
    protected $entitySet;
    protected $dal;

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getEntitySet()
    {
        return $this->entitySet;
    }

    /**
     * @param mixed $entitySet
     */
    public function setEntitySet($entitySet)
    {
        $this->entitySet = $entitySet;
    }

    public function __construct($dal = null)
    {
        $this->entitySet = array();
        $this->dal = $dal;
        $this->entity = $dal->getEntity();
    }
}