<?php
namespace ModernWays\Helpers\Bll\Entity;
/**
 * Class BaseOld.php
 * een materieel of immaterieel object, een exemplaar of een instantie van een entiteittype
 * @package ModernWays\Helpers\Bll
 */
class Base
{
	protected $insertedBy;
	protected $insertedOn;
	protected $updatedBy;
	protected $updatedOn;

	// system fields
	// We moeten een onderscheid maken tussen getters en
	// setters die automatisch worden aangemaakt en getters en setters
	// die tot de klasse behoren.
	// log field
	protected $_log;
	protected $_validated;

	public function __construct($_log)
	{
		$this->log = $_log;
		$this->_validated = true;
 	}

	// system methods
	// validation method resturns log
	// if log is empty, no errors
	public function isValid()
	{
		return $this->_validated;
	}

	// entity methods
	public function getInsertedBy()
	{
		return $this->insertedBy;
	}

	public function getInsertedOn()
	{
		return $this->insertedOn;
	}

	public function getUpdatedBy()
	{
		return $this->updatedBy;
	}

	public function getUpdatedOn()
	{
		return $this->updatedOn;
	}


	public function setInsertedBy($value)
	{
		$this->insertedBy = $value;
	}

	public function setInsertedOn()
	{
		$this->insertedOn = date("Y-m-d H:i:s");
	}

	public function setUpdatedBy($value)
	{
		$this->updatedBy = $value;
	}

	public function setUpdatedOn()
	{
		$this->updatedOn = date("Y-m-d H:i:s");
	}
}

?>


