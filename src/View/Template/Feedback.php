<h3><?php echo $this->model->getName(); ?></h3>
<div>
    <label>Calling class: </label>
    <span><?php echo $this->model->getClassName(); ?></span>
</div>
<div>
    <label>Feedback text: </label>
    <span><?php echo $this->model->getText(); ?></span>
</div>
<div>
    <label>Context: </label>
    <span><?php echo $this->model->getContext(); ?></span>
</div>
<div>
    <label>Error code: </label>
    <span><?php echo $this->model->getErrorCode(); ?></span>
</div>
<div>
    <label>Error message: </label>
    <span><?php echo $this->model->getErrorMessage(); ?></span>
</div>
<div><label>Error Code Driver: </label>
    <span><?php echo $this->model->getErrorCodeDriver(); ?></span>
</div>
<div>
    <label>Is error?: </label>
    <span><?php echo $this->model->getIsError(); ?></span>
</div>
<div><label>Start: </label>
    <span><?php echo $this->model->getStartTime(); ?></span>
</div>
<div><label>End: </label>
    <span><?php echo $this->model->getEndTime(); ?></span>
</div>
<div><label>Debug info: </label>
    <span><?php echo $this->model->getDebugInfo();?></span>
</div>
