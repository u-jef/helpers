<div class="logbook">
    <h1><?php echo $this->model->getTitle(); ?></h1>
    <?php
    if (count($this->model->getBook()) > 0)
    {
        $view = new \ModernWays\Helpers\View\Feedback();
        foreach ($this->model->getBook() as $key => $feedback)
        {
            // var_dump($feedback);
            $view->setModel($feedback);
            $view->output();
        }
    }
    else
    {
        ?>
        <h3>No feedback</h3>
        <?php
    }
    ?>
