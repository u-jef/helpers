<?php
/**
 * Created by Modern Ways.
 * Author: Jef Inghelbrecht
 * Date: 20/07/2015
 * Time: 12:32
 */
namespace ModernWays\Helpers\View;
class Base
{
    protected $model;

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    public function __construct($model = null)
    {
        if (isset($model)) {
            $this->model = $model;
        }
    }
}