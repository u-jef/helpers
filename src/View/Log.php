<?php
/**
 * Created by Modern Ways.
 * Template: Jef Inghelbrecht
 * Date: 20/07/2015
 * Time: 12:32
 */
namespace ModernWays\Helpers\View;
class Log
{
    private $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function output()
    {
        include('Template/Log.php');
    }

}