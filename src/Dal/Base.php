<?php
    /**
        Dal Base class
        @lastmodified 17 januari 2015
        @author JI
        @version 1.0
    */

    // name of namespace should be semantically meaningfull:
    // cover the domain of the code
namespace ModernWays\Helpers\Dal;
class Base
{
	protected $entity;
	protected $connection;
	// log field
	protected $log;
	protected $rowCount;
	// log getter and setter

	public function __construct($log, $entity, $connection)
	{
		$this->log = $log;
        $this->entity = $entity;
        $this->connection = $connection;
	}

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param mixed $connection
     */
    public function setConnection($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getRowCount()
    {
        return $this->rowCount;
    }

    /**
     * @param mixed $rowCount
     */
    public function setRowCount($rowCount)
    {
        $this->rowCount = $rowCount;
    }



}



