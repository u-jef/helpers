<?php
    /**
        Connection class
        @since 10 april 2012
        @lastmodified 15 april 2015
        @author JI
        @version 1.0
    */

    // name of namespace should be semantically meaningfull:
    // cover the domain of the code
    namespace ModernWays\Helpers\Dal;

    use ModernWays\Helpers\LogLocale;

    class Connection
    {
        // design means identifying data and methods
        protected $log;
        protected $databaseName;
        protected $pdo;
        protected $userName;
        protected $hostName;
        protected $password;

        /**
         * @return mixed
         */
        public function getLog()
        {
            return $this->log;
        }

        /**
         * @param mixed $log
         */
        public function setLog($log)
        {
            $this->log = $log;
        }

        /**
         * @return mixed
         */
        public function getDatabaseName()
        {
            return $this->databaseName;
        }

        /**
         * @param mixed $databaseName
         */
        public function setDatabaseName($databaseName)
        {
            $this->databaseName = $databaseName;
        }

        /**
         * @return mixed
         */
        public function getPdo()
        {
            return $this->pdo;
        }

        /**
         * @param mixed $pdo
         */
        public function setPdo($pdo)
        {
            $this->pdo = $pdo;
        }

        /**
         * @return mixed
         */
        public function getUserName()
        {
            return $this->userName;
        }

        /**
         * @param mixed $userName
         */
        public function setUserName($userName)
        {
            $this->userName = $userName;
        }

        /**
         * @return mixed
         */
        public function getHostName()
        {
            return $this->hostName;
        }

        /**
         * @param mixed $hostName
         */
        public function setHostName($hostName)
        {
            $this->hostName = $hostName;
        }

        /**
         * @return mixed
         */
        public function getPassword()
        {
            return $this->password;
        }

        /**
         * @param mixed $password
         */
        public function setPassword($password)
        {
            $this->password = $password;
        }

	    public function isConnected()
	    {
            return ($this->pdo ? TRUE : false);
	    }

        // constructor wordt uitgevoerd met
        // het new keyword
        public function __construct($log)
        {
            $this->log = $log;
        }

        /**
        * Maakt een connectie met de database
        * @return Bool true als de connectie is gelukt, false als mislukt
        */
        public function open()
        {
            $this->log->startTimeInKey('open connection');
             if ($this->pdo)
            {
                $text = $this->log->connection($this->hostName, $this->databaseName, LogLocale::CONNECTION_ESTABLISHED);
                $this->log->setText($text);
                $this->log->log();
            }
            else
            {
                try
                {
                    $connectionString = 
                        "mysql:host={$this->hostName};dbname={$this->databaseName}";
                    // je moet aangeven dat de PDO klasse in de root namespace
                    // gezocht moet worden in niet in MyBib\Dal
                    $this->pdo = new \PDO($connectionString, $this->userName, $this->password);
                    $text = $this->log->connection($this->hostName, $this->databaseName, LogLocale::CONNECTION_OPENED);
                    $this->log->setText($text);
 			        $this->log->setErrorCodeDriver('ModernWays DAL Connction');
                    $this->log->log();
                }
                catch (\PDOException $e)
                {
                    $text = $this->log->connection($this->hostName, $this->databaseName, LogLocale::CONNECTION_FAILED);
                    $this->log->setText($text);
 				    $this->log->setErrorMessage('Fout: ' . $e->getMessage());
				    $this->log->setErrorCode($e->getCode());
 			        $this->log->setErrorCodeDriver('ModernWays DAL Connection');
                    $this->log->end();
                    $this->log->log();
               }
            }
            return (!is_null($this->pdo));
        }

        public function close()
        {
            // $this->log->clear();
            $this->log->startTimeInKey('close connection');
            if (is_null($this->pdo))
            {
                $text = $this->log->connection($this->hostName, $this->databaseName, LogLocale::CONNECTION_NOT_CONNECTED);
                $this->log->setText($text);
                $this->log->log();
            }
            else
            {
                $this->pdo = NULL;
                $text = $this->log->connection($this->hostName, $this->databaseName, LogLocale::CONNECTION_CLOSED);
                $this->log->setText($text);
                $this->log->end();
                $this->log->log();
            }
        }
    }

