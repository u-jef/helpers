<?php
/**
 * Created by Modern Ways.
 * Template: Jef Inghelbrecht
 * Date: 21/07/2015
 * Time: 11:21
 */
namespace ModernWays\Helpers\Dal;

class AppProvider extends Connection
{
    private $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function __construct($log, $name)
    {
        $this->log = $log;
        $this->name = $name;
        switch ($this->name) {
            case 'OvhWindowsMikMak' :
                $this->databaseName = 'MikMak';
                $this->password = 'xxxxxxxxxxxxxxxx/';
                $this->userName = 'user';
                $this->hostName = 'host.com:3306';
                break;
            case 'MikMakLocalMobielePC' :
                $this->databaseName = 'MikMak';
                $this->password = '!xxxxxxxx';
                $this->userName = 'root';
                $this->hostName = 'localhost:3306';
                break;
            case 'MembershipMobielePC' :
                $this->databaseName = 'Membership';
                $this->password = 'xxxxxxxxxxxxx';
                $this->userName = 'root';
                $this->hostName = 'localhost:3306';
                break;
            case 'MembershipHuiskamerPC' :
                $this->databaseName = 'Membership';
                $this->password = 'xxxxxxxxxxxxxxxxxxxxx';
                $this->userName = 'root';
                $this->hostName = 'localhost:3306';
                break;
            case 'OpenShift' :
                $this->databaseName = 'WWWET';
                $this->password = getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
                $this->userName = getenv("OPENSHIFT_MYSQL_DB_USERNAME");
                $this->hostName = getenv("OPENSHIFT_MYSQL_DB_HOST");
                break;
            case 'OvhUbuntuMikMak' :
                $this->databaseName = 'MikMak';
                $this->password = 'xxxxxxxxxxxxxxx';
                $this->userName = 'mikmak';
                $this->hostName = 'host.com';
                break;
            default :
                $this->databaseName = 'MikMak';
                $this->password = 'xxxxxxxxxxxxxxxxxxx';
                $this->userName = 'root';
                $this->hostName = 'localhost:3306';
                break;
        }
    }

}


