<?php
/**
 * Created by ModernWays.
 * User: Jef Inghelbrecht
 * Date: 18/07/2015
 * Time: 20:53
 * The Front Controller consolidates all request handling by channeling requests
 * through a single handler object. This object can carry out common behavior,
 * which can be modified at runtime with decorators.
 * The handler then dispatches to command objects for behavior particular
 * to a request. (Martin Fowler)
 *
 * In our sample we intercept a get or a post as a usecase definition.
 * The usecase is first parse. Then a controller is dynamically created
 * and the appropriated method is called.
 */
namespace ModernWays\Helpers;
/**
 * Class UseCase
 * @package ModernWays\Helpers
 */
class FrontController
{
    protected $entity; /* the noun of the use case, object or instance */
    protected $id;
    protected $action; /* the verb of the use case */
    protected $clause;
    protected $useCase; /* the name of the use case */
    protected $log;
    protected $controller;

    public function isAction()
    {
        return (!is_null($this->action));
    }

    public function isClause()
    {
        return (!is_null($this->clause));
    }

    public function isUseCase()
    {
        return (!is_null($this->useCase));
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getClause()
    {
        return $this->clause;
    }

    public function getUseCase()
    {
        return $this->useCase;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function setUseCase($value)
    {
        $this->useCase = $value;
        $this->parse();
    }

    function __construct($log)
    {
        $this->log = $log;
    }

    public function requestUseCase()
    {
        // default action is home
        $this->useCase = '-home--';
        // what action to take?
        // if action is specified on querystring
        // change default action
        if (isset($_POST['uc'])) {
            $this->useCase = $_POST['uc'];
        } elseif (isset($_GET['uc'])) {
            // sommige acties komen via een get binnen op de url
            $this->useCase = $_GET['uc'];
        }
        $this->parse();
    }

    private function parse()
    {
        // if number attached
        $pos = strpos($this->useCase, '_');
        if ($pos !== FALSE) {
            $this->id = substr($this->useCase, $pos + 1);
            $this->useCase = substr($this->useCase, 0, $pos);
        }
        // action from one - up to double --
        $pos = strpos($this->useCase, '--');
        // echo $pos;
        if ($pos == FALSE) {
            $this->useCase = $this->useCase . '--';
        }
        $pos = strpos($this->useCase, '--');
        $startPos = strpos($this->useCase, '-');
        $this->action = substr($this->useCase, $startPos + 1, $pos - $startPos - 1);
        // look for entity (table) name, up to first -
        $pos = strpos($this->useCase, '-');
        if ($pos !== FALSE) {
            $this->entity = substr($this->useCase, 0, $pos);
        }
        // clause from double -- to end
        $pos = strpos($this->useCase, '--');
        if ($pos !== FALSE) {
            $this->clause = substr($this->useCase, $pos + 2);
        }
        $this->log->startTimeInKey('set action');
        $this->log->setText("Entitity = {$this->entity} / Action = {$this->action} / Clause = {$this->clause} / Id = {$this->id}");
        $this->log->setErrorMessage("IsActionSet = {$this->isAction()}");
        $this->log->end();
        $this->log->log();
    }

    public function createController($namespace)
    {
        $controllerName = $namespace . $this->entity;
        $this->log->startTimeInKey('Create Controller');
        $this->log->setText("Creating the action controller '$controllerName'...");
        if (!class_exists($controllerName, true)) {
            $this->log->setErrorMessage("The action controller '$controllerName' has not been defined.");
            $this->log->end();
            $this->log->log();
            return false;
        }
        else {
            $reflection = new \ReflectionClass($controllerName);
            $this->controller =  $reflection->newInstance();
            $this->log->setErrorMessage("The action controller '$controllerName' object has been created.");
            $this->log->end();
            $this->log->log();
            return true;
        }
    }
}